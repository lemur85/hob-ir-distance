/**
 * @file HOB_SharpIR.h
 * @author Eloy Soto [lemur85] <el.lemur.85@gmail.com>
 * @date 23/12/2015
 * @brief File containing HOB_SharpIR class definition.
 */

#ifndef __HOB_SHARP_IR_H__
#define __HOB_SHARP_IR_H__

#include "Arduino.h"

class HOB_SharpIR {
public:
	HOB_SharpIR(uint8_t sensorPin);
	uint8_t GetDistance(void);
	void SetSensorCalibration(int8_t sensorCal);
	void SetAlarmDistance(uint8_t distance);
	boolean GetAlarm(void);

private:
	uint8_t _sensorPin = A0;
	int8_t _sensorCalibration = 0;
	uint8_t _alarmDistance = 0;
	const uint8_t _SamplesNb = 4;

};

#endif /* __HOB_SHARP_IR_H__ */
