/**
 * @file HOB_SharpIR.cpp
 * @author Eloy Soto [lemur85] <el.lemur.85@gmail.com>
 * @date 23/12/2015
 * @brief File containing HOB_SharpIR class implementation.
 *
 * This class implements the House Of Bots SHARP IR distance sensor controller logic.
 *
 */

#include "HOB_SharpIR.h"

/**
 * @brief Class constructor
 * Class constructor, takes analog pin where we have connected the sensor.
 *
 * @param[in] sensorPin Input analog pin where sensor is connected.
 */
HOB_SharpIR::HOB_SharpIR(uint8_t sensorPin) {
	_sensorPin = sensorPin;
	_alarmDistance = 0;
	pinMode(_sensorPin, INPUT);
}


/**
 * @brief Get distance (in centimeters) function
 * This function takes 4 samples of analog pin where sensor is connected obtaining
 * the average value for better resolution, and returns the measured distance in
 * centimeters.
 *
 * @returns Measured distance in centimeters.
 */
uint8_t HOB_SharpIR::GetDistance(void) {
	int analogValue = 0;
	unsigned int total = 0;
	uint8_t idx = 0;

	for(idx=0; idx<_SamplesNb; idx++) {
		total += analogRead(_sensorPin);
		delay(2);
	}

	analogValue = total/_SamplesNb;

	return (uint8_t)((pow(3027.4 / analogValue, 1.2134)) + _sensorCalibration);
}

/**
 * @brief Set alarm distance function
 * This function sets an alarm distance in centimeters.
 *
 * @param[in] distance Alarm distance in centimeters.
 *
 */
void HOB_SharpIR::SetAlarmDistance(uint8_t distance) {
	_alarmDistance = distance;
}

/**
 * @brief Get alarm function
 * This function returns TRUE if measured distance is less or equal to
 * that indicated using "SetAlarmDistance" function. The default alarm
 * distance is zero centimeters.
 *
 * @returns TRUE if measured distance is less or equal to previously indicated
 * 			threshold (using 'SetAlarmDistance' method). FALSE if it's greater.
 *
 */
boolean HOB_SharpIR::GetAlarm(void) {
	return (boolean)(GetDistance() <= _alarmDistance);
}

/**
 * @brief Set calibration value function
 * This function allow us to specify a calibration value. This value would be added to
 * the distance measurement result. For example, we can specify '-1' if we see that
 * our sensor is measuring '+1 cm' on distance result. By default, calibration value
 * is set to zero.
 *
 */
void HOB_SharpIR::SetSensorCalibration(int8_t sensorCal) {
	_sensorCalibration = sensorCal;
}
