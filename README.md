# README #

House Of Bots library for SHARP 2Y0A21 IR distance sensor control. Have fun!


### What is this repository for? ###

This library allow us to control [SHARP 2Y0A21 IR distance sensor](http://www.ebay.com/itm/Arduino-SHARP-Sensor-GP2Y0A21YK0F-10-80cm-With-Cable-2Y0A21-GP2D12-/271576329103)

Implemented functionality:
* GetDistance: Returns measured distance in cm.
* SetSensorCalibration: Sets a calibration value for measured distance.
* SetAlarmDistance: Sets an alarm distance value.
* GetAlarm: Tells us if configured alarm distance threshold is reached.

## Install ##

To use this library you must download the repository (or clone it), and copy the "HOB_SharpIR" directory to your arduino sketchbook library path (for example, on "/home/*USER*/sketchbook/libraries". 

It's all, you should now see it on "Sketch -> Import Library" menu on Arduino IDE.

For more information visit our blog [House of Bots](https://houseofbots.wordpress.com/)


## Usage example ##

Connect the sensor to Arduino board. Looking at the front part of the sensor (where measuring LEDs are placed), the left pin is the sensor output, middle pin is GND and right pin is VCC. To use this example, connect the sensor as indicated in the initial code comment.

```
#!Arduino
/**
 * House Of Bots HOB_SharpIR example.
 *
 * Connections:
 *  - Sensor output to Arduino A0 pin
 *  - Sensor GND to Arduino GND pin
 *  - Sensor VCC to Arduino VCC 3.3v pin
 *
 * Visit https://houseofbots.wordpress.com
 *
 */

#include <HOB_SharpIR.h>

/* Sharp IR controller */
HOB_SharpIR irSensor(A0);

/* Setup serial output */
void setup(void) {
  Serial.begin(9600);
}

void loop(void) {
  uint8_t idx = 0;
  
  /* First, we take 30 distance measurements */
  for(idx=0;idx<=30; idx++) {
    Serial.print(irSensor.GetDistance());
    Serial.println(" cm");
    delay(1500);
  }
  
  /* Alarm usage example */
  irSensor.SetAlarmDistance(10);
  
  while(1) {
    if(irSensor.GetAlarm() == true) {
      Serial.println("ALARM!!");
    } else {
      Serial.println("Not alarm");
    }
    
    delay(1000);
  }
}
```


### Contact ###

* Repo admin: lemur85 <el.lemur.85@gmail.com>
* Other community or team contact: House Of Bots <houseofbots@gmail.com>